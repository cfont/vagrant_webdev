#import.md

Files into this import folder will be used to auto-load backups of sites into the development environment.  After backups are used, they will be moved to a generated subfolder to avoid accidental data loss.

## Example of backup types
* Drush archive dump tar files