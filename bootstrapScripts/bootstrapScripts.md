#bootstrapScripts.md
The scripts below are used in various combinations to auto-build test environments within the vagrant box.  Uncomment the corresponding lines within the vagrantSettings.yaml to add the desired functionality
## CentOS
### CentOS_LAMP.sh
* Provisions basic LAMP installation and configuration for CentOS.
## Webapp
### cleanDrupalInstall.sh
* Downloads and installs latest stable Drupal 7.x core
* Default user:pass is vagrant:vagrant
### cleanWordpressInstall.sh
* Downloads and installs latest stable Wordpress core
* Default user:pass is vagrant:vagrant
### developerSuite.sh
* Downloads and installs various toolsets (Code Sniffer, Behat, Selenium, Drush, wp-cli, etc.) 
### tarDrupalLoad.sh
* Performs a drush archive restore from the latest tar file found withing the import folder
* The restore will overwrite all files within the public_html directory of the project
* No default username:password set.  Whatever Drupal users were in the previous environment will be present
## MySQL
### sqlLoad.sh
* Drops local "Web" MySQL database and imports from latest sql file found in import folder