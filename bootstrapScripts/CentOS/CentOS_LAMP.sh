#!/usr/bin/env bash
# CentOS_LAMP.sh
#
#LAMP provisioning script

	#Configure OS
	yum update -y
	yum install -y lynx php-soap
	gem update --system
	gem install bundler
	

	#Configure Apache
		echo "Creating symlinks"
			rm -R /var/www/html
			ln -fs /vagrant/public_html/ /var/www/html
			ln -fs /vagrant/public_html/ /home/vagrant/www

		echo "Configuring Apache"
			sed -i 's/apc.shm_size=64M/apc.shm_size=256M/' /etc/php.d/apc.ini
			echo "ServerName website.vbox.local" >> /etc/httpd/conf/httpd.conf
			sed -i 's/#NameVirtualHost/NameVirtualHost/' /etc/httpd/conf/httpd.conf
			sed -i 's/User apache/User vagrant/' /etc/httpd/conf/httpd.conf
			sed -i 's/Group apache/Group vagrant/' /etc/httpd/conf/httpd.conf
			sed -i 's/AllowOverride None/AllowOverride All/' /etc/httpd/conf/httpd.conf
			sed -i 's/#EnableSendfile off/EnableSendfile off/' /etc/httpd/conf/httpd.conf
			sed -i 's/128M/256M/' /etc/php.ini
			sed -i 's/;date.timezone =/date.timezone = America\/New_York/' /etc/php.ini
			printf 'start on vagrant-mounted\nexec sudo service httpd start' > /etc/init/vagrant-mounted.conf
			service httpd restart


	#Configure MySQL Server
		echo "Installing MySQL"
			yum -y install mysql mysql-server
			service mysqld restart
			/usr/bin/mysqladmin -u root password 'root'
			mysql -u root -e 'create database Web' -proot
			sed '/^symbolic-links/a max_allowed_packet=512M' /etc/my.cnf > /etc/my2.cnf
			rm /etc/my.cnf
			mv /etc/my2.cnf /etc/my.cnf
			printf '[client]\n\tuser=root\n\thost=localhost\n\tpassword=root' > /home/vagrant/.my.cnf
			chown vagrant:vagrant /home/vagrant/.my.cnf

	#Setup startup applications
		chkconfig mysqld on
		chkconfig httpd on

	#Restart services
		service mysqld restart
		service httpd restart


	echo "OS Completed Provisioning"