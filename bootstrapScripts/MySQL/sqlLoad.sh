#!/bin/bash
#sqlLoad.sh
#
#Drops Web database and imports from latest sql file found in import folder

# Variables
	IFLocation=/vagrant/import #Place drush archive dumps into this import folder
	webroot=/vagrant/public_html #Webroot for website.vbox.local
	
# Find newest sql file previously exported from a drush ARD command
	cd $IFLocation
	IF=$(ls -t *.sql | head -1)
	clear
	touch importHistory.txt
	
	
# Move all sql files, newest to imported folder, all other sql files to notImported.  Avoids accidental re-provisions from overwriting data
	if [ "$IF" = "" ]; then
	{	echo "No sql files found.  No import file processed."
		echo $(date)  "No sql files found.  No import file processed." >> importHistory.txt
		exit
	}
	else
	{
		# Create import folders for later cleanup locations
			mkdir -p $IFLocation/imported
			mkdir -p $IFLocation/notImported
		
		# Drop/ recreate existing Web MySQL database
			mysql -u root -proot -e "DROP DATABASE IF EXISTS Web"
			mysql -u root -proot -e "CREATE DATABASE Web"

		# Configure MySQL for large imports		
			echo "Setting MYSQL Packet Size to 500M"
			mysql -u root -proot -e "set global max_allowed_packet = 500 * 1024 * 1024; "
			
		# Perform Import
			cd ~	#Needed to clear shell-init error after using mysql and then attempting other commands
			cd $IFLocation
			echo "Performing SQL Import from "$IF
			mysql -u root -proot Web < $IF
			mv $IF $IFLocation/imported	

		# Cleanup import files

			$(mv $IFLocation/*.sql $IFLocation/notImported >/dev/null 2>&1)	#move all other sql files to notImported location to avoid accidental overwrite on future provisionings
			echo $(date) "Import" $IF "into MySQL Completed"
			echo $(date) "Imported" $IF "into MySQL" >> $IFLocation/importHistory.txt
	}
	fi