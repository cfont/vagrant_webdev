#!/usr/bin/env bash
# cleanWordpressInstall.sh
#
# Wordpress latest auto-install script, derived from alienation 24 jan 2013


# Download wordpress to temporary area
  cd /tmp
  rm -rf tmpwp
  mkdir tmpwp
  cd tmpwp
  wget http://wordpress.org/latest.tar.gz
  tar -xvzpf latest.tar.gz

# Copy wordpress to where it will live, and go there
  rm -rf /vagrant/public_html
  mkdir /vagrant/public_html
  mv wordpress/* /vagrant/public_html
  cd /vagrant/public_html


# Create config from sample, replacing salt example lines with a real salt from online generator
  grep -A 1 -B 50 'since 2.6.0' wp-config-sample.php > wp-config.php
  wget -O - https://api.wordpress.org/secret-key/1.1/salt/ >> wp-config.php
  grep -A 50 -B 3 'Table prefix' wp-config-sample.php >> wp-config.php

# Replace appropriate db info in place of placeholders in our new config file
  replace 'database_name_here' 'Web' -- wp-config.php
  replace 'username_here' 'root' -- wp-config.php
  replace 'password_here' 'root' -- wp-config.php

# Configure Wordpress Site
  cd /vagrant/public_html
  su vagrant -c "cd /vagrant/public_html; /usr/local/bin/wp-cli core  install --url=http://website.vbox.local --title=website.vbox.local --admin_user=vagrant --admin_password=vagrant --admin_email=siteadmin@website.vbox.local"


echo "Wordpress installation complete. Visit the homepage to finish the setup configuration."
