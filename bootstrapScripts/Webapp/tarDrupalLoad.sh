#!/bin/bash
#drupalLoad.sh
#
#Loads Drupal Site from latest tar file found in import folder

# Variables
	IFLocation=/vagrant/import #Place drush archive dumps into this import folder
	webroot=/vagrant/public_html #Webroot for website.vbox.local
	siteConfig=$webroot/sites/default/settings.php
	theme=bmc #Change this to desired theme for configuring bundler gemfile 

# Find newest tar file previously exported from a drush ARD command
	cd $IFLocation
	IF=$(ls -t *.tar | head -1)
	clear
	touch importHistory.txt
	
	
# Move all tar files, newest to imported folder, all other tar files to notImported.  Avoids accidental re-provisions from overwriting data
	if [ "$IF" = "" ]; then
	{	echo "No drush tar files found.  No import file processed."
		echo $(date)  "No drush tar files found.  No import file processed." >> importHistory.txt
		exit
	}
	else
	{
		# Create import folders for later cleanup locations
			mkdir -p $IFLocation/imported
			mkdir -p $IFLocation/notImported
			mv $IF $IFLocation/imported	
		
		# Configure MySQL for large imports		
			echo "Setting MYSQL Packet Size to 500M"
			mysql -u root -proot -e "set global max_allowed_packet = 500 * 1024 * 1024; "
			
		# Perform Import
			cd ~	#Needed to clear shell-init error after using mysql and then attempting drush commands
			cd $webroot
			echo "Performing Drupal Import from "$IF
			drush arr $IFLocation/imported/$IF --destination=$webroot --db-url=mysql://root:root@localhost/Web --overwrite --db-su=root --db-su-pw=root -v
		
		# Cleanup local installation settings
			sed -i 's/$production = TRUE;/$production = FALSE;/' $siteConfig
			head --lines=-16 $siteConfig > $webroot/sites/default/settings2.php
			rm $siteConfig
			mv $webroot/sites/default/settings2.php $siteConfig
			sed -i "/'database'/ c\\\t\t\t'database' => 'Web'", $siteConfig
			sed -i  "/'username'/ c\\\t\t\t'username' => 'root'", $siteConfig
			sed -i  "/'password'/ c\\\t\t\t'password' => 'root'", $siteConfig
			sed -i  "/'host'/ c\\\t\t\t'host' => 'localhost'", $siteConfig

		# Turn off all caching/ aggregation
			cd $webroot
			drush pm-disable varnish -y
			drush vset cache 0
			drush vset block_cache 0
			drush vset preprocess_css 0
			drush vset preprocess_js 0
			drush cc all
		
		# Configure bundler gemfile

			if [ -f $webroot/sites/all/themes/$theme/Gemfile ];	then
			{
				cd $webroot/sites/all/themes/$theme
				echo "Gemfile located.  Initializing bundler gem."
				bundle install
			}    
			fi

		# Cleanup import files

			$(mv $IFLocation/*.tar $IFLocation/notImported >/dev/null 2>&1)	#move all other tar files to notImported location to avoid accidental overwrite on furture provisionings
			echo $(date) "Import" $IF "into Drupal Completed"
			echo $(date) "Imported" $IF "into Drupal" >> $IFLocation/importHistory.txt
	}
	fi