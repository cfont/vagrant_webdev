#!/usr/bin/env bash
#drupalInstall.sh
#
#Drupal provisioning Script


#Download and extract Drupal 7.x
	echo "Beginning Drupal Provisioning"
	cd /vagrant/public_html
	drush dl drupal-7.x
	mv /vagrant/public_html/drupal-7.x-dev/* /vagrant/public_html
	mv /vagrant/public_html/drupal-7.x-dev/.htaccess /vagrant/public_html
	mv /vagrant/public_html/drupal-7.x-dev/.gitignore /vagrant/public_html
	rm -R /vagrant/public_html/drupal-7.x-dev
	drush si --db-url=mysql://root:root@localhost/Web --account-name=vagrant --account-pass=vagrant --clean-url --site-name=website.vbox.local -y
	echo "$conf['drupal_http_request_fails'] = FALSE;" >> /vagrant/public_html/sites/default/settings.php
	mkdir /vagrant/public_html/sites/all/modules/contrib
	mkdir /vagrant/public_html/sites/all/modules/custom
	cd /vagrant/public_html
	drush dl module_filter
	drush en module_filter -y
	echo "Site Ready! Login to the webpage with vagrant:vagrant"
