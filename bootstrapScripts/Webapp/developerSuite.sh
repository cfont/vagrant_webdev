#!/bin/bash
# developerSuite.sh
#
# Installs PHP CodeSniffer, Coder, Behat, Selenium, Drush, wp-cli

  yum update -y

# Fix Centos 6 root user PATH bug: https://bugs.centos.org/view.php?id=5707
  printf 'export PATH="/usr/local/bin:$PATH"' >> /root/.bashrc

#Install CLI tools
  cd /tmp
  wget http://dl.fedoraproject.org/pub/epel/6/i386/epel-release-6-8.noarch.rpm
  rpm -Uvh epel-release*rpm
  yum install -y php-drush-drush
  wget https://raw.githubusercontent.com/wp-cli/builds/gh-pages/phar/wp-cli.phar
  chmod +x wp-cli.phar
  mv wp-cli.phar /usr/local/bin/wp-cli
  mkdir /home/vagrant/.drush/dump -p
  chown vagrant:vagrant /home/vagrant/.drush -R

# Install ImageMagick
  yum install -y ImageMagick ImageMagick-perl

# Behat/ Selenium (system-wide installation)
  echo "Installing Selenium and related tools"
    cd /home/vagrant
    wget http://selenium-release.storage.googleapis.com/2.47/selenium-server-standalone-2.47.1.jar
    mv /home/vagrant/selenium-server-standalone-2.47.1.jar /usr/bin/selenium
    yum install -y firefox Xvfb xorg-x11-fonts* xauth
    printf '#!/bin/bash\n\nkillall java\nkillall xvfb-run\nkillall Xvfb\nkillall firefox\nrm /home/vagrant/selenium.log\n\nDISPLAY=localhost:10 xvfb-run java -jar /usr/bin/selenium > /home/vagrant/selenium.log&\n' > /home/vagrant/selenium-xvfb.sh
    chmod +x /home/vagrant/selenium-xvfb.sh
    chown vagrant:vagrant /home/vagrant/selenium-xvfb.sh

  echo "Installing Behat, Coder, and PHP CodeSniffer"
    cd /home/vagrant
    curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/local/bin --filename=composer
    mkdir /opt/drupalextension -p
    cd /opt/drupalextension
    printf '{\n\t"require": {\n\t\t"drupal/coder": "^8.2",\n\t\t"drupal/drupal-extension": "~3.0",\n\t\t"behat/mink": "1.5.*@stable",\n\t\t"behat/mink-goutte-driver": "*",\n\t\t"behat/mink-selenium2-driver": "*"\t\n\t},\n\t"config": {\n\t\t"bin-dir": "bin/"\n\t}\n}' > /opt/drupalextension/composer.json
    cd /opt/drupalextension/
    php /usr/local/bin/composer install

# Create symlinks to applications
  ln -s /opt/drupalextension/bin/behat /usr/local/bin/behat
  ln -s /opt/drupalextension/bin/phpcs /usr/local/bin/phpcs
  ln -s /opt/drupalextension/bin/phpcbf /usr/local/bin/phpcbf

# Configure CodeSniffer
  echo "Configuring CodeSniffer"
    printf 'alias drupalcs="phpcs --standard=Drupal --extensions='\''php,module,inc,install,test,profile,theme,js,css,info,txt'\''"' >> /home/vagrant/.bashrc
    printf 'alias drupalcbf="phpcs --standard=Drupal"' >> /home/vagrant/.bashrc
    phpcs --config-set installed_paths /opt/drupalextension/vendor/drupal/coder/coder_sniffer

# Restart services
  service mysqld restart
  service httpd restart