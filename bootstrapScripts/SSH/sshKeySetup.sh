#!/usr/bin/env bash
#ssKeySetup.sh
#
#SSH Key and directory provisioning script
#Todo: Enumerate servers list automatically from servers.cfg sourced file


# Server arrays are as follows: Name, ip or hostname, port number, username
# Create a local servers.cfg file with more environments, and append them to the servers array below
local=("local" "127.0.0.1" "2222" "vagrant")
source ./servers.cfg

servers=(local[@])
#servers=(local[@] testing[@] production[@]) #Add new elements based on servers.cfg file entries
count=${#servers[@]}

for ((remote=0; remote<$count; remote++ ))
do
	host=(${!servers[remote]})
	name=${host[0]}
	hostname=${host[1]}
	port=${host[2]}
	username=${host[3]}

	echo "----------------------------"
	echo "Settings for $name"
	echo "Hostname $hostname"
	echo "Port $port"
	echo "Username $username"
	echo "-                          -"
	
	echo "Performing actions on $name"
	echo "Creating .ssh directory, if missing"
	ssh "-p $port" $username"@"$hostname "mkdir /home/$username/.ssh"
	echo "Append public key"
	cat "$HOME/.ssh/authorized_keys"| ssh "-p $port" $username"@"$hostname "cat >> /home/$username/.ssh/authorized_keys"
	echo "Fix file permissions"
	ssh "-p $port" $username"@"$hostname "chmod 700 /home/$username/.ssh"
	ssh "-p $port" $username"@"$hostname "chmod 600 /home/$username/.ssh/authorized_keys"
	echo "----------------------------"
done
