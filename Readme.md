#Vagrant_WebDev

A Vagrant project that easily auto-builds web development environments
 
##Setup
- Install [Virtualbox](https://www.virtualbox.org/wiki/Downloads)
- Install Virtualbox's Extension Pack
- Install [Vagrant](http://www.vagrantup.com/downloads.html)
- Install [Git](http://www.git-scm.com/downloads)
- Install [Vagrant-vbguest plugin](https://github.com/dotless-de/vagrant-vbguest)
- Install [Vagrant-hostmanager plugin](https://github.com/smdahlen/vagrant-hostmanager)
- Clone this repo: `git clone https://bitbucket.org/backyardclubhouse/vagrant_webdev.git .`
- Open vagrantSettings.yml for editing
	* `servers-WebApp-virtualbox-name`:  Changes the guest name listed within VirtualBox.
	* `servers-WebApp-virtualbox-gui`: Enables the guest os console window.  Useful for troubleshooting boot issues
	* `servers-WebApp-virtualbox-memory`: Changes the guest OS memory
	* Please see the bootstrapScripts.md file for more information on the available bootstrapScripts.
- In terminal (or Git Bash on Windows), change directory to the project folder and type `vagrant up`


##Using the Project
After build is complete, the virtual machine can be accessed via the follow methods:

- `vagrant ssh` will ssh from the terminal to the guest OS
- Connect ssh to 127.0.0.1:2222 will redirect to the guest OS.  This only works from the host machine.
- Open a browser on the host OS, directed at http://website.vbox.local


##User Accounts: User Name/ Password
Use | Username | Password
:----:|:----:|:----:
Guest OS User (ssh, sudoer, etc.) | vagrant | vagrant
MySQL user | root | root

##Vagrant Command Quick Reference
Vagrant commands use the syntax `$ vagrant COMMAND`.  To find a list of some available commands, type `vagrant`.  For help with a command, use `vagrant COMMAND -h`.